import axios from "axios";
import { useAuthStore } from "./stores/AuthStore";



const axiosClient = axios.create({
    baseURL: 'https://asistencia.dilushop.com.co/api/v1/',
    headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
    }
});

axiosClient.interceptors.request.use(config => {
    const authStore = useAuthStore();
    
    if(authStore.user.token){
        config.headers.Authorization = 'Bearer ' + authStore.user.token
    }
    return config;
})

export default axiosClient;