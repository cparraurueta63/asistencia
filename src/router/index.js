import { createRouter, createWebHistory } from "vue-router";
import { useAuthStore } from "../stores/AuthStore";
import Login from '../views/auth/Login.vue';
import Register from '../views/auth/Register.vue';
import EmployeeIndex from '../views/employee/EmployeeIndex.vue';
import AttendanceIndex from '../views/attendance/AttendanceIndex.vue'

const routes = [
    {
        path: '/',
        name: 'paginaBlanco',
        redirect: '/iniciar-sesion',
    },
    {
        path: '/iniciar-sesion',
        name: 'iniciarSesion',
        component: Login
    },
    {
        path: '/registrarse',
        name: 'registrarse',
        component: Register
    },
    {
        path: '/empleados',
        name: 'empleados',
        meta: { requiresAuth: true },
        component: EmployeeIndex
    },
    {
        path: '/asistencias',
        name: 'asistencias',
        meta: { requiresAuth: true },
        component: AttendanceIndex
    },
]


const router = createRouter({
    history: createWebHistory(),
    routes
});



router.beforeEach((to, from, next) => {
    const authStore = useAuthStore();

    if (authStore.user.token && (to.name === 'iniciarSesion' || to.name === 'registrarse')) {
        // Si el usuario está autenticado y está intentando acceder a iniciarSesion o registrarse, redirige a /empleados
        next("/empleados");
    } else if (to.meta.requiresAuth && !authStore.user.token) {
        // Si la ruta requiere autenticación y el usuario no está autenticado, redirige a /iniciar-sesion
        next("/iniciar-sesion");
    } else {
        // Permite la navegación normal
        next();
    }
});

export default router;