import { defineStore } from "pinia";
import axiosClient from "../axios";

export const useAttendanceStore = defineStore({
    id: 'AttendanceStore',

    state: () => ({
        attendances: []
    }),

    actions: {
        async registerAttendance(form) {
            const response = await axiosClient.post('/attendances', form, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                }
            });
            return response;
        },
        async getAttendances(form) {
            form._method = 'GET';
            const response = await axiosClient.post('/attendances', form);
            this.attendances = response.data.data;
            return response;
        },
    },

});