import { defineStore } from "pinia";
import axiosClient from "../axios";
import router from "../router";

export const useAuthStore = defineStore({
    id: 'AuthStore',

    state: () => ({
        user: {
            data: {},
            token: sessionStorage.getItem("TOKEN"),
        }
    }),

    actions: {
        async getUser() {
            const response = await axiosClient.get('/user')
            this.user.data = response.data;
            return response;
        },
        async login(form) {
            const response = await axiosClient.post('/login', form)
            this.user.data = response.data.data.user;
            this.user.token = response.data.data.token;
            sessionStorage.setItem('TOKEN', response.data.data.token);
            router.push('/empleados');
            return response;
        },

        async register(form) {
            const response = await axiosClient.post('/register', form)
            this.user.data = response.data.data.user;
            this.user.token = response.data.data.token;
            sessionStorage.setItem('TOKEN', response.data.data.token);
            router.push('/empleados');
            return response;

        },

        async logout() {
            const response = await axiosClient.post('/logout');
            this.user.data = null;
            this.user.token = null;
            sessionStorage.removeItem('TOKEN');
            router.push('/iniciar-sesion');
            return response;
        }
    },

});