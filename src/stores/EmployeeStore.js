import { defineStore } from "pinia";
import axiosClient from "../axios";

export const useEmployeeStore = defineStore({
    id: 'EmployeeStore',

    state: () => ({
        employees: []
    }),

    actions: {
        async createEmployee(form) {
            const response = await axiosClient.post('/employees', form, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                }
            });
            return response;
        },
        async getEmployees(form) {
            form._method = 'GET';
            const response = await axiosClient.post('/employees', form);
            this.employees = response.data.data;
            return response;
        },

        async deleteEmployee(employee) {
            const response = await axiosClient.delete(`/employees/${employee}`)
            return response;
        },

        async updateEmployee(employee, form) {
            form._method = 'PUT';
            const response = await axiosClient.post(`/employees/${employee}`, form, {
                headers: {
                    'Content-Type': 'multipart/form-data',
                }
            });
            return response;
        },

        async getEmployeeReport() {
            const response = await axiosClient.get('/employee/report');
            return response;
        },



    },

});